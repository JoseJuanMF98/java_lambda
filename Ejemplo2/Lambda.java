/*
Interfaz Funcional
*/
public class Lambda{
public static void main(String[] args){
//expresion Lambda ==> representa un objeto de una interfaz funcional

Operaciones op = (num1, num2) -> System.out.println (num1 + num2);
//operacion multiplicacion usando el mismo metodo con diferente objeto
Operaciones op2 = (num1, num2) -> System.out.println (num1 * num2);

op.imprimeSuma (5,10);
op2.imprimeSuma (5,10);
}
}