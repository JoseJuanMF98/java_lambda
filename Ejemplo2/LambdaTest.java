/*
Interfaz Funcional
*/
public class LambdaTest{
public static void main(String[] args){
//expresion Lambda ==> representa un objeto de una interfaz funcional

Operaciones op = (num1, num2) -> System.out.println (num1 + num2);
//operacion multiplicacion usando el mismo metodo con diferente objeto
Operaciones op2 = (num1, num2) -> System.out.println (num1 * num2);

LambdaTest obj = new LambdaTest();
obj.miMetodo (op, 2,10);
obj.miMetodo (op2, 2,10);

}
public void miMetodo(Operaciones op,int num1, int num2){
op.imprimeSuma (num1, num2);
}
}