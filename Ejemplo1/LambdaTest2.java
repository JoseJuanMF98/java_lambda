/*
Interfaz Funcional
*/
public class LambdaTest2{
public static void main(String[] args){
//expresion Lambda ==> representa un objeto de una interfaz funcional

FunctionTest ft = () -> System.out.println ("Hola mundo"); //Implementacion del método abstracto "saludar()" de la interfaz funcional

FunctionTest gt = () -> System.out.println ("Hola Dos mundos");

LambdaTest2 obj = new LambdaTest2();
obj.miMetodo(ft);
obj.miMetodo(gt);
}

public void miMetodo(FunctionTest parametro){
parametro.saludar();
}
}