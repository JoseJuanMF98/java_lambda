/*
Interfaz Funcional
*/
public class Lambda{
public static void main(String[] args){
//expresion Lambda ==> representa un objeto de una interfaz funcional

FunctionTest ft = () -> System.out.println ("Hola mundo"); //Implementacion del método abstracto "saludar()" de la interfaz funcional
FunctionTest gt = () -> System.out.println ("Dos mundos");



ft.saludar();
gt.saludar();

}
}